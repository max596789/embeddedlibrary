/* 
 * File:   isqrt.h
 * Author: Michael15
 *
 * Created on June 16, 2015, 3:14 PM
 */

#ifndef ISQRT_H
#define	ISQRT_H

unsigned long isqrt(unsigned long n);

#endif	/* ISQRT_H */

