/**
 * @defgroup uart UART Module
 * @file uart.h
 *
 *  Created on: Mar 12, 2014
 *      Author: Michael Muhlbaier
 *  Updated on: Feb 7, 2015
 *      Author: Anthony Merlino
 * @{
 */

#ifndef _UART_H_
#define _UART_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include "system.h"
#include "charReceiverList.h"

/** NOTE:  This enumeration has to occur BEFORE the include for
 * the hal_uart.h because hal_uart.h includes this uart.h file
 * so that it can see this error enumeration.
 */
typedef enum {
        BREAK_ERROR = 0,
        PARITY_ERROR,
        FRAMING_ERROR,
        OVERRUN_ERROR,
        NUM_UART_ERRORS
} UART_ERROR;

/**************************************
 * Hardware Abstraction Layer Includes
 *************************************/
#include "hal_uart.h"

/** Initialize UART module
 *
 * Example usage:
 * @code
 * UART_Init(UART0_CHANNEL);
 * @endcode
 *
 * @param channel - The channel of UART to be used.  Macros for these should be defined in the
 * HAL of the specific device.
 */
void UART_Init(uint8_t channel);

void UART_ReconfigureBaud(uint8_t channel, uint32_t baud);

void UART_WriteByte(uint8_t channel, char c);

/**
 * @brief Write chunk of data to UART tx buffer
 *
 * @param channel - the UART channel to send on
 * @param data - a pointer to the data to send
 * @param length - the amount of data to send
 * @return 0 = success, -1 = not enough room in buffer, -2 = Invalid channel
 */
int8_t UART_Write(uint8_t channel, char * data, uint16_t length);
void UART_Printf(uint8_t channel, char * str,...);
void UART_vprintf(uint8_t channel, char * str, va_list vars);
uint8_t UART_IsTransmitting(uint8_t channel);

void UART_Tick(void);
error_t UART_RegisterReceiver(uint8_t channel, charReceiver_t fn);
void UART_UnregisterReceiver(uint8_t channel, charReceiver_t fn);

void UART_RegisterErrorCallback(uint8_t channel, void(*callback)(UART_ERROR));

void UART_RegisterTxOverwriteCallback(uint8_t channel, void(*overwriteCallback)(void));

void UART_RegisterRxOverwriteCallback(uint8_t channel, void(*overwriteCallback)(void));

/** @}*/
#endif /* _UART_H_ */
