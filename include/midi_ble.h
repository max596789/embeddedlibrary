/** 
 * File:   midi_ble.h
 * Author: Michael
 *
 * Created on March 22, 2016, 2:57 PM
 * 
 * @defgroup midi_ble MIDI BLE using Apple's Spec
 * @ingroup midi
 * @{
 */

#ifndef MIDI_BLE_H
#define	MIDI_BLE_H

#include "stdint.h"
#include "midi.h"

#ifdef	__cplusplus
extern "C" {
#endif

/**
 * 
 */
void MIDI_BLE_Init(void);

/**
 * 
 * @param data
 * @param length
// needs to generate and update 13bit ms timestamp
// each BLE packet is:
// [header][timestamp 1][midi msg][timestamp 2][midi msg 2]...
// except for system exclusive message which is:
// [header][timestamp][midi msg]
// [header][rest of midi msg without EOX][timestamp][EOX]
 * */
void MIDI_BLE_Send(uint8_t data[], uint8_t length);

/**
 * 
 * @param data
 */
void MIDI_BLE_Receive(uint8_t data);

/*
 * Run the add service function in a way that work with the task scheduler
 */
static void DelayedGATTAddService128(void);

/*
 * Run the add characteristic function in a way that work with the task scheduler
 */
static void DelayedGATTAddChar128(void);

/*
 * Run the set GAP advertising data function in a way that work with the task scheduler
 */
static void DelayedGAPSetAdvData(void);

/*
 * Run the MIDI init function in a way that work with the task scheduler
 */
static void DelayedMIDI_Init(void);

/*
 * Convert integers to ascii hex values
 * @param data -- the integer to convert
 */
static char int2hex(uint8_t data);

#ifdef	__cplusplus
}
#endif
/** @} */
#endif	/* MIDI_BLE_H */
