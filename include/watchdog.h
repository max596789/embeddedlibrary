/**
 * File:   watchdog.h
 * Author: Michael15
 *
 * Created on July 29, 2015, 11:07 AM
 * 
 * Use Watchdog_Init() to initialize the module then use Watchdog_Reset() to
 * reset the watchdog timer. Watchdog_Reset() will only set a flag and then a
 * scheduled task will reset the actual watchdog timer. This will ensure that
 * the Task Management module is running properly.
 * 
 * @defgroup watchdog Watchdog Timer for use with Task Management Module
 */

#ifndef _WATCHDOG_H_
#define	_WATCHDOG_H_

#ifdef	__cplusplus
extern "C" {
#endif
#include "hal_general.h"
    
/** @brief Timeout of the 
 */
#ifndef WATCHDOG_TIMEOUT_IN_MS
#define WATCHDOG_TIMEOUT_IN_MS 1000
#endif

    /// alias to provide backwards compatibility with the old name
#define SetResetWatchdogFlag() Watchdog_Reset()
    /// alias to provide backwards compatibility with the old name
#define ResetWatchdog() Watchdog_Reset()
    
/** @brief initialize the module which means scheduling a task to reset the
 * watchdog timer.
 * 
 */
void Watchdog_Init(void);

/** @brief Set a flag to have the watchdog timer reset.
 */
void Watchdog_Reset(void);

/** Hardware specific clear watchdog timer. Normally defined as a macro in
 * hal_general.h
 */
#ifndef hal_Watchdog_Clear
void hal_Watchdog_Clear(void);
#endif

/** Hardware specific enable watchdog timer. Normally defined as a macro in
 * hal_general.h
 * If the watchdog has no software enable then just define hal_Watchdog_Enable():
 * @code
 * #define hal_Watchdog_Enable();
 * @endcode
 */
#ifndef hal_Watchdog_Enable
void hal_Watchdog_Enable(void);
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* WATCHDOG_H */

