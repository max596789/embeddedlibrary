/*
 * game_racer.c
 *
 *  Created on: Mar 21, 2016
 *      Author: Jacob Cooper
 */

#include "system.h"
#include "random_int.h"
#include "strings.h"


#define MAP_WIDTH 40
#define MAP_HEIGHT 30

#define MAX_CAR_SIZE 3
#define MIN_ENEMY_TIME 3000/(MAP_HEIGHT-1)
#define MAX_ENEMY_TIME 12000/(MAP_HEIGHT-1)
#define MIN_ENEMY_RATE 1000
#define MAX_ENEMY_RATE 8000

#define MIN_RACER_RATE 1000
#define MAX_RACER_RATE 300

/// game structure
struct racer_game_t {
    char x; ///< x coordinate of racer
    char y; ///< y coordinate of racer
    uint16_t s; ///< speed of racer
    char c; ///< character of racer
    int cars_passed; ///< number of enemy cars passed
    int score; ///< score for the round
    uint8_t id; ///< ID of game=
};

char racecar[MAX_CAR_SIZE][MAX_CAR_SIZE] ={
		{'0','H','0'},
		{' ','H',' '},
		{'0','H','0'}
};

#define RACER_MAX_ENEMIES 3
char_object_t enemies[RACER_MAX_ENEMIES];
struct racer_game_t game;

uint8_t state = 0;

static void Callback(int argc, char * argv[]);
static void Receiver(char c);

static void PrePlay(void);
static void Play(void);
static void Help(void);
static void MoveDownEnemy(char_object_t * o);
static void MoveRightEnemy(char_object_t * o);

static void DrawRacer(void);
static void DrawEnemy(void);
static void SendEnemy(void);
static void GameOver(void);
static void MoveRight(void);
static void MoveLeft(void);
static void MoveUp(void);
static void MoveDown(void);
static void SpeedUp(void);
static void SpeedDown(void);
static void MoveBoard(uint16_t rate);
static void ReDrawTrack(void);
static void CheckCollisionEnemy(void);
static void CheckPassEnemy(void);
static void DeleteEnemy(char_object_t * o);
static void DeleteShot(char_object_t * o);
void ScorePoint(void);

void RACER_Init(void) {
    // Register the module with the game system and give it the name "MUH3"
    game.id = Game_Register("racer", "racing game", PrePlay, Help);
    // Register a callback with the game system.
    // this is only needed if your game supports more than "play", "help" and "highscores"
    Game_RegisterCallback(game.id, Callback);
}

void Help(void) { Game_Printf("Instructions print prior to game play\r\n"); }


void Callback(int argc, char * argv[]) {
    // "play" and "help" are called automatically so just process "reset" here
    if(argc == 0) Game_Log(game.id, "too few args");
    if(strcasecmp(argv[0],"reset") == 0) {
        // reset scores
        game.score = 0;
        Game_Log(game.id, "Scores reset");
    }else Game_Log(game.id, "command not supported");
}

void PrePlay(void) {
    Game_Printf("\f'a' and 'd' or '<' and '>' to move your car left and right\r\n"
            "'w' and 's' to move your car up and down\r\n"
            "Game will begin in 5 seconds.\n\r");
    // Use the Task Management Module to schedule the function Play() to run
    // 5000ms from now and set period to 0 so it never runs again (e.g. one time scheduled task).
    Task_Schedule(Play, 0, 5000, 0);
}

void Play(void) {
    volatile uint8_t i;
    // clear the screen
    Game_ClearScreen();
    // draw a box around our map
    Game_DrawRect(0, 0, MAP_WIDTH, MAP_HEIGHT);

    // initialize game variables
    for(i = 0; i < RACER_MAX_ENEMIES; i++) enemies[i].status = 0;
    game.x = MAP_WIDTH / 2;
    game.y = MAP_HEIGHT / 2;
    game.s = MIN_RACER_RATE;
    game.c = '^';
    game.score = 0;
    game.cars_passed = 0;
    // draw the "car" at the bottom center of the map
    Game_CharXY(game.c, game.x, game.y);
    //DrawRacer();
    Game_RegisterPlayer1Receiver(Receiver);
    // hide cursor
    Game_HideCursor();
    // send first enemy in 10 seconds
    // period is 0 because the task is rescheduled new each time at random intervals
    Task_Schedule(SendEnemy, 0, 5000, 0);
}

void Receiver(char c) {
    switch (c) {
        case 'a':
        case ',':
        case 'A':
        case '<':
            MoveLeft();
            break;
        case 'd':
        case 'D':
        case '.':
        case '>':
            MoveRight();
            break;
        case 'w':
        case 'W':
        	SpeedUp();
        	break;
        case 's':
        case 'S':
        	SpeedDown();
        	break;
        case ' ':
            //Fire(); gear up or gear down
            break;
        case '\r':
            GameOver();
            break;
        default:
            break;
    }
}

/* void Fire(void) {
    static tint_t last_fired;
    volatile uint8_t i;
    // pointer to bullet to use
    char_object_t * shot = 0;
    // make sure a shot was not just fired
    if (TimeSince(last_fired) <= FIRE_SPEED) return;

    // find and unused bullet
    for(i = 0; i < MUH3_MAX_SHOTS; i++) if(shots[i].status == 0) shot = &shots[i];
    if(shot) {
        // schedule MoveUpShot1 to run every FIRE_SPEED ms, this is what makes the '^' move
        Task_Schedule((task_fn_t)MoveUpShot, shot, FIRE_SPEED, FIRE_SPEED);
    }else { // if no shots are left
        // indicate out of ammo by changing the pointer
        if (game.c == '^') {
            game.c = 'o';
            Game_CharXY(game.c, game.x, MAP_HEIGHT - 1);
        }
        return;
    }
    // change the status of the shot to indicate that it is used
    shot->status = 1;
    // start the shot at the bottom just above the gun
    shot->y = MAP_HEIGHT - 2;
    shot->x = game.x;
    shot->c = '^';
    Game_CharXY('^', game.x, MAP_HEIGHT - 2);
    // track the time the last shot was fired so you can't have two on top of each other
    last_fired = TimeNow();
    game.shots_fired++;
}*/

void DrawRacer(void){
    volatile int i, j;
	for(i = -MAX_CAR_SIZE/2; i < MAX_CAR_SIZE - MAX_CAR_SIZE/2; i++){
		for(j = -MAX_CAR_SIZE/2; j < MAX_CAR_SIZE - MAX_CAR_SIZE/2; j++){
			game.x += i;
			game.y += j;
			Game_CharXY(' ', game.x, game.y);
			Game_CharXY(racecar[i+MAX_CAR_SIZE][j+MAX_CAR_SIZE], game.x, game.y);
		}
	}
}
void DrawEnemy(void){}

void SendEnemy(void) {
    char x;
    volatile uint8_t i;
    char_object_t * enemy = 0;
    uint16_t down_period, right_period;
    // get random starting x location
    x = random_int(1, MAP_WIDTH / 2);
    // get random time for moving down
    down_period = random_int(MIN_ENEMY_TIME, MAX_ENEMY_TIME);
    // get random time for moving right such that we don't go out of bounds
    // min value is t*(MAP_HEIGHT-1) / (MAP_WIDTH - 1 - x)
    right_period = down_period * (MAP_HEIGHT - 1) / (MAP_WIDTH - 1 - x);
    // get value between the min and 4x min
    right_period = random_int(right_period, right_period * 4);
    // find unused enemy object
    for(i = 0; i < RACER_MAX_ENEMIES; i++) if(enemies[i].status == 0) enemy = &enemies[i];
   if(enemy) {
        enemy->status = 1;
        enemy->x = x;
        enemy->c = 'x';
        enemy->y = 1;
        // use the task scheduler to move the char down and right at the calculated
        // period
        Task_Schedule((task_fn_t)MoveDownEnemy, enemy, down_period, down_period);
        Task_Schedule((task_fn_t)MoveRightEnemy, enemy, right_period, right_period);
        Game_CharXY('x', x, 1);
    }
    // schedule next enemy to come between min and max enemy rate
    // since a random time is desired the period is 0 so the task will be done when it us run
    // and a new task will be created at a different random time
    Task_Schedule(SendEnemy, 0, random_int(MIN_ENEMY_RATE, MAX_ENEMY_RATE), 0);
}

void MoveRight(void) {
	if(game.s != MIN_RACER_RATE){
		// make sure we can move right
		if (game.x < MAP_WIDTH - 1) {
			// clear location
			Game_CharXY(' ', game.x, game.y);
			game.x++;
			// update
			Game_CharXY(game.c, game.x, game.y);
		   // DrawRacer();
		}
	}
}

void MoveLeft(void) {
	if(game.s != MIN_RACER_RATE){
		// make sure we can move right
		if (game.x > 1) {
			// clear location
			Game_CharXY(' ', game.x, game.y);
			game.x--;
			// update
			Game_CharXY(game.c, game.x, game.y);
			//DrawRacer();
		}
	}
}

void MoveUp(void){
	//make sure we can move up
	if(game.y > 5){
		//clear location
		Game_CharXY(' ', game.x, game.y);
		game.y--;
		// update
		Game_CharXY(game.c, game.x, game.y);
		//DrawRacer();
	}
}

void MoveDown(void){
	//make sure we can move down
	if(game.y < MAP_HEIGHT - 5){
		//clear location
		Game_CharXY(' ', game.x, game.y);
		game.y++;
		// update
		Game_CharXY(game.c, game.x, game.y);
		//DrawRacer();
	}
}

void SpeedUp(){
	if(game.s > MAX_RACER_RATE){
		game.s -= 30;
		MoveBoard(game.s);
		MoveUp();
	}
}


void SpeedDown(){
	if(game.s < MIN_RACER_RATE){
		game.s += 30;
		MoveBoard(game.s);
		MoveDown();
	}else{
		MoveBoard(MIN_RACER_RATE);
	}
}

void MoveBoard(uint16_t rate){
	//Task_Schedule(SendEnemy, 0, random_int(MIN_ENEMY_RATE, MAX_ENEMY_RATE), 0);
	if(rate == MIN_RACER_RATE){
		Task_Remove(ReDrawTrack, 0);
		return;
	}

	Task_Remove(ReDrawTrack, 0);
	Task_Schedule(ReDrawTrack, 0,rate/2, rate);
}

void ReDrawTrack(void){
	// clear the screen
    Game_ClearScreen();

    //re-draw racecar
    Game_CharXY(game.c, game.x, game.y);
    //DrawRacer();
    //re-draw enemies

    char x_min = 0;
    char y_min = 0;
    char x_max = MAP_WIDTH;
    char y_max = MAP_HEIGHT;
    state ^= 1;
	char y;
	for (y = y_min; y < y_max; y++){
		if(state){
			if(y%2 == 0)
				Game_CharXY(186, x_max, y);
			else
				Game_CharXY(' ', x_max, y);
		}
		else{
			if(y%2 != 0)
				Game_CharXY(186, x_max, y);
			else
				Game_CharXY(' ', x_max, y);
		}
	}
	// wait to transmit all the chars before continuing
	// (this could be removed if the TX buffer size is increased)
	// another way around this would be to schedule the rest of the printout
	// to happen several ms from now
	while (Game_IsTransmitting()) DelayMs(2);

	for (y = y_min; y < y_max; y++) {
		if(state){
			if(y%2 == 0)
				Game_CharXY(186, x_min, y);
			else
				Game_CharXY(' ', x_min, y);
		}else{
			if(y%2 != 0)
				Game_CharXY(186, x_min, y);
			else
				Game_CharXY(' ', x_min, y);
		}
	}
}

void MoveDownEnemy(char_object_t * o) {
    // first make sure we can move down
    if (o->y < MAP_HEIGHT - 1) {
        // clear location
        Game_CharXY(' ', o->x, o->y);
        // update y position
        o->y++;
        // re-print
        Game_CharXY(o->c, o->x, o->y);
        CheckCollisionEnemy();
        CheckPassEnemy();
    } else {
    	DeleteEnemy(o);
    }
}

void MoveRightEnemy(char_object_t * o) {
    // first make sure we can move right
    if (o->x < MAP_WIDTH - 1) {
        // clear location
        Game_CharXY(' ', o->x, o->y);
        // update x position
        o->x++;
        // re-print
        Game_CharXY(o->c, o->x, o->y);
        CheckCollisionEnemy();
        CheckPassEnemy();
    } else {
    	DeleteEnemy(o);
    }
}

void MoveUpShot(char_object_t * o) {
    // first make sure we can move up
    if (o->y > 1) {
        // clear location
        Game_CharXY(' ', o->x, o->y);
        // update y position
        o->y--;
        // re-print
        Game_CharXY(o->c, o->x, o->y);
        //CheckCollisionShot(o);
    } else {
        // clear the shot
        Game_CharXY(' ', o->x, o->y);
        DeleteShot(o);
    }
}

void CheckPassEnemy() {
    volatile uint8_t i;
    char_object_t * enemy;
    for(i = 0; i < RACER_MAX_ENEMIES; i++) {
        enemy = &enemies[i];
        if(enemy->status) {
            if (enemy->y == game.y) {
            	ScorePoint();
            }
        }
    }
}

void CheckCollisionEnemy() {
    volatile uint8_t i;
    char_object_t * enemy;
    for(i = 0; i < RACER_MAX_ENEMIES; i++) {
        enemy = &enemies[i];
        if(enemy->status) {
            if (enemy->x == game.x && enemy->y == game.y) {
                DeleteEnemy(enemy);

                // put a * where the collision happened
                Game_CharXY('*', enemy->x, enemy->y);
                GameOver();
            }
        }
    }
}

void ScorePoint(void) {
    game.score++;
    game.cars_passed += 10;
    // sound the alarm
    Game_Bell();
}

// if o is 0 then delete all enemies
void DeleteEnemy(char_object_t * o) {
    // set status to 0
    if(o) o->status = 0;
    // remove the tasks used to move the enemy
    Task_Remove((task_fn_t)MoveDownEnemy, o);
    Task_Remove((task_fn_t)MoveRightEnemy, o);
}

// if o is 0 then delete all shots
void DeleteShot(char_object_t * o) {
    // set status to 0
    if(o) o->status = 0;
    // remove the tasks used to move the shot
    Task_Remove((task_fn_t)MoveUpShot, o);

    // if there were no shots left then change the pointer back to ^
    if (game.c != '^') {
        game.c = '^';
        Game_CharXY(game.c, game.x, MAP_HEIGHT - 1);
    }
}

void GameOver(void) {
    // clean up all scheduled tasks
    DeleteEnemy(0);
    Task_Remove(ReDrawTrack, 0);
    volatile uint8_t i;
    char_object_t * enemy = 0;
    for(i = 0; i < RACER_MAX_ENEMIES; i++){
    	if(enemies[i].status == 0){
    		enemy = &enemies[i];
    		Task_Remove(SendEnemy, 0);
			Task_Remove((task_fn_t)MoveDownEnemy, enemy);
			Task_Remove((task_fn_t)MoveRightEnemy, enemy);
    	}
    }
    // set cursor below bottom of map
    Game_CharXY('\r', 0, MAP_HEIGHT + 1);
    // show score
    Game_Printf("Game Over! Final score: %d\r\nTotal cars passed: %d", game.cars_passed , game.score);
    // unregister the receiver used to run the game
    Game_UnregisterPlayer1Receiver(Receiver);
    // show cursor (it was hidden at the beginning
    Game_ShowCursor();
}

