/*
 * DAq.c
 *
 *  Created on: Mar 27, 2015
 *      Author: Bradley Ebinger
 */
#include <stdint.h>

#include "../include/daq.h"

#define NULL_POINTER 0

// Holds Samples for Daq IDs
typedef struct {
	uint8_t ID;
	// Alternating acquisition buffer for interrupt-safe operation
	// Pointer to buffer being used to record data
	uint8_t *bufptr;
	// Empty buffer location
	uint16_t bufIndex;
	uint8_t rawBuf1[DAQ_BUFFER_SIZE];
	uint8_t rawBuf2[DAQ_BUFFER_SIZE];
} daq_raw_data_t;

// Linked list of
typedef struct daq_data_node_t{
	daq_raw_data_t *daq_data;
	struct daq_data_node_t *next;
} daq_data_node_t;

daq_data_node_t data_list;

void DAQ_init(void){

	// Create a head pointer for the linked list.
	data_list.daq_data = NULL_POINTER;
	data_list.next = NULL_POINTER;
}

void DAQ_RegisterDevice(daq_id_data_t *device){
	daq_data_node_t *node = &data_list;

	while(node->next != NULL_POINTER) {
		if(device->ID == node->daq_data->ID){
			// The device already exists in the list!
			break;
		}
		node = node->next;
	}

}

void DAQ_AcquireData(daq_id_data_t *device, uint8_t *data, uint8_t dataLen){

}

void DAQ_ForceDataDump(daq_id_data_t *device){

}

void DAQ_UpdateTick(void){

}
