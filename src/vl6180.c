/*
 * vl6180.c
 *
 *  Created on: Mar 11, 2015
 *      Author: Anthony
 *
 *  Adapted from code found @ https://github.com/sparkfun/ToF_Range_Finder-VL6180_Library/
 *  by Casey Kuhns @ SparkFun Electronics
 */

#include "vl6180.h"
#include "system.h"

static int8_t sys_id = -1;

int8_t VL6180_Init(vl6180_t* vl6180_handle){
  uint8_t data; //for temp data storage

  if(sys_id == -1){
	 sys_id = Subsystem_Init("VL6180", (version_t) 0x01010001U, 0);
  }

  //The VL6180 always defaults to address 0x29, so make sure the handle has the default address
  vl6180_handle->address = VL6180_DEFAULT_ADDRESS;

  data = VL6180_GetRegister(vl6180_handle, VL6180_SYSTEM_FRESH_OUT_OF_RESET);

  if(data != 1) return VL6180_FAILURE_RESET;

  //Required by datasheet
  //http://www.st.com/st-web-ui/static/active/en/resource/technical/document/application_note/DM00122600.pdf
  VL6180_SetRegister(vl6180_handle, 0x0207, 0x01);
  VL6180_SetRegister(vl6180_handle, 0x0208, 0x01);
  VL6180_SetRegister(vl6180_handle, 0x0096, 0x00);
  VL6180_SetRegister(vl6180_handle, 0x0097, 0xfd);
  VL6180_SetRegister(vl6180_handle, 0x00e3, 0x00);
  VL6180_SetRegister(vl6180_handle, 0x00e4, 0x04);
  VL6180_SetRegister(vl6180_handle, 0x00e5, 0x02);
  VL6180_SetRegister(vl6180_handle, 0x00e6, 0x01);
  VL6180_SetRegister(vl6180_handle, 0x00e7, 0x03);
  VL6180_SetRegister(vl6180_handle, 0x00f5, 0x02);
  VL6180_SetRegister(vl6180_handle, 0x00d9, 0x05);
  VL6180_SetRegister(vl6180_handle, 0x00db, 0xce);
  VL6180_SetRegister(vl6180_handle, 0x00dc, 0x03);
  VL6180_SetRegister(vl6180_handle, 0x00dd, 0xf8);
  VL6180_SetRegister(vl6180_handle, 0x009f, 0x00);
  VL6180_SetRegister(vl6180_handle, 0x00a3, 0x3c);
  VL6180_SetRegister(vl6180_handle, 0x00b7, 0x00);
  VL6180_SetRegister(vl6180_handle, 0x00bb, 0x3c);
  VL6180_SetRegister(vl6180_handle, 0x00b2, 0x09);
  VL6180_SetRegister(vl6180_handle, 0x00ca, 0x09);
  VL6180_SetRegister(vl6180_handle, 0x0198, 0x01);
  VL6180_SetRegister(vl6180_handle, 0x01b0, 0x17);
  VL6180_SetRegister(vl6180_handle, 0x01ad, 0x00);
  VL6180_SetRegister(vl6180_handle, 0x00ff, 0x05);
  VL6180_SetRegister(vl6180_handle, 0x0100, 0x05);
  VL6180_SetRegister(vl6180_handle, 0x0199, 0x05);
  VL6180_SetRegister(vl6180_handle, 0x01a6, 0x1b);
  VL6180_SetRegister(vl6180_handle, 0x01ac, 0x3e);
  VL6180_SetRegister(vl6180_handle, 0x01a7, 0x1f);
  VL6180_SetRegister(vl6180_handle, 0x0030, 0x00);

  return 0;
}

void VL6180_DefautSettings(vl6180_t* vl6180_handle){
  //Recommended settings from datasheet
  //http://www.st.com/st-web-ui/static/active/en/resource/technical/document/application_note/DM00122600.pdf

  //Enable Interrupts on Conversion Complete (any source)
  VL6180_SetRegister(vl6180_handle, VL6180_SYSTEM_INTERRUPT_CONFIG_GPIO, (4 << 3)|(4) ); // Set GPIO1 high when sample complete

  VL6180_SetRegister(vl6180_handle, VL6180_SYSTEM_MODE_GPIO1, 0x10); // Set GPIO1 high when sample complete
  VL6180_SetRegister(vl6180_handle, VL6180_READOUT_AVERAGING_SAMPLE_PERIOD, 0x30); //Set Avg sample period
  VL6180_SetRegister(vl6180_handle, VL6180_SYSALS_ANALOGUE_GAIN, 0x46); // Set the ALS gain
  VL6180_SetRegister(vl6180_handle, VL6180_SYSRANGE_VHV_REPEAT_RATE, 0xFF); // Set auto calibration period (Max = 255)/(OFF = 0)
  VL6180_SetRegister(vl6180_handle, VL6180_SYSALS_INTEGRATION_PERIOD, 0x63); // Set ALS integration time to 100ms
  VL6180_SetRegister(vl6180_handle, VL6180_SYSRANGE_VHV_RECALIBRATE, 0x01); // perform a single temperature calibration
  //Optional settings from datasheet
  //http://www.st.com/st-web-ui/static/active/en/resource/technical/document/application_note/DM00122600.pdf
  VL6180_SetRegister(vl6180_handle, VL6180_SYSRANGE_INTERMEASUREMENT_PERIOD, 0x09); // Set default ranging inter-measurement period to 100ms
  VL6180_SetRegister(vl6180_handle, VL6180_SYSALS_INTERMEASUREMENT_PERIOD, 0x0A); // Set default ALS inter-measurement period to 100ms
  VL6180_SetRegister(vl6180_handle, VL6180_SYSTEM_INTERRUPT_CONFIG_GPIO, 0x24); // Configures interrupt on �New Sample Ready threshold event�
  //Additional settings defaults from community
  VL6180_SetRegister(vl6180_handle, VL6180_SYSRANGE_MAX_CONVERGENCE_TIME, 0x32);
  VL6180_SetRegister(vl6180_handle, VL6180_SYSRANGE_RANGE_CHECK_ENABLES, 0x10 | 0x01);
  VL6180_SetRegister16bit(vl6180_handle, VL6180_SYSRANGE_EARLY_CONVERGENCE_ESTIMATE, 0x7B );
  VL6180_SetRegister16bit(vl6180_handle, VL6180_SYSALS_INTEGRATION_PERIOD, 0x64);

  VL6180_SetRegister(vl6180_handle, VL6180_READOUT_AVERAGING_SAMPLE_PERIOD,0x30);
  VL6180_SetRegister(vl6180_handle, VL6180_SYSALS_ANALOGUE_GAIN,0x40);
  VL6180_SetRegister(vl6180_handle, VL6180_FIRMWARE_RESULT_SCALER,0x01);
}

void VL6180_GetDistance(vl6180_t* vl6180_handle){
	// If the sensor is not setup for continuous then trigger the sensor and wait
	if(vl6180_handle->settings.continuous != VL6180_RANGE_CONTINUOUS){
		//Start Single shot mode
		VL6180_SetRegister(vl6180_handle, VL6180_SYSRANGE_START, 0x01);
		WaitMs(10);
	}
	vl6180_handle->distance = VL6180_GetRegister(vl6180_handle, VL6180_RESULT_RANGE_VAL);
}

void VL6180_SetRangePeriod(vl6180_t* vl6180_handle, uint8_t period_x10ms){
	if(period_x10ms > 254){
		period_x10ms = 254;
	}
	VL6180_SetRegister(vl6180_handle, VL6180_SYSRANGE_INTERMEASUREMENT_PERIOD, period_x10ms);
}

void VL6180_StartContinuous(vl6180_t* vl6180_handle){
	VL6180_SetRegister(vl6180_handle, VL6180_SYSRANGE_START, 0x03);
	vl6180_handle->settings.continuous = VL6180_RANGE_CONTINUOUS;
}

void VL6180_StopContinuous(vl6180_t* vl6180_handle){
	VL6180_SetRegister(vl6180_handle, VL6180_SYSRANGE_START, 0x01);
	vl6180_handle->settings.continuous = VL6180_RANGE_SINGLE;
}

uint8_t VL6180_SetAddress(vl6180_t* vl6180_handle, uint8_t address){

	if( vl6180_handle->address == address) return vl6180_handle->address;
	if( address > 127) return vl6180_handle->address;

	VL6180_SetRegister(vl6180_handle, VL6180_I2C_SLAVE_DEVICE_ADDRESS, address);

	vl6180_handle->address = address;

	return vl6180_handle->address;
}

uint8_t VL6180_GetRegister(vl6180_t* vl6180_handle, uint16_t regAddr){
	i2c_transaction_t transaction;

	transaction.slave_address = vl6180_handle->address;
	transaction.blocking = 1;
	transaction.channel = VL6180_I2C;
	transaction.retryCount = 2;
	transaction.callback = 0;
	transaction.writeData[0] = ((regAddr >> 8) & 0xFF);
	transaction.writeData[1] = (regAddr & 0xFF);
	transaction.writeLength = 2;
	transaction.readLength = 1;

	I2C_Transact(&transaction);

	return transaction.readData[0];
}

uint16_t VL6180_GetRegister16bit(vl6180_t* vl6180_handle, uint16_t regAddr){
	i2c_transaction_t transaction;

	transaction.slave_address = vl6180_handle->address;
	transaction.blocking = 1;
	transaction.channel = VL6180_I2C;
	transaction.retryCount = 2;
	transaction.callback = 0;
	transaction.writeData[0] = ((regAddr >> 8) & 0xFF);
	transaction.writeData[1] = (regAddr & 0xFF);
	transaction.writeLength = 2;
	transaction.readLength = 2;

	I2C_Transact(&transaction);

	return ((transaction.readData[0] << 8) | transaction.readData[1]);
}

void VL6180_SetRegister(vl6180_t* vl6180_handle, uint16_t regAddr, uint8_t data){
	i2c_transaction_t transaction;

	transaction.slave_address = vl6180_handle->address;
	transaction.blocking = 1;
	transaction.channel = VL6180_I2C;
	transaction.retryCount = 2;
	transaction.callback = 0;
	transaction.writeData[0] = ((regAddr >> 8) & 0xFF);
	transaction.writeData[1] = (regAddr & 0xFF);
	transaction.writeData[2] = data;
	transaction.writeLength = 3;
	transaction.readLength = 0;

	I2C_Transact(&transaction);
}

void VL6180_SetRegister16bit(vl6180_t* vl6180_handle, uint16_t regAddr, uint16_t data){
	i2c_transaction_t transaction;

	transaction.slave_address = vl6180_handle->address;
	transaction.blocking = 0;
	transaction.channel = VL6180_I2C;
	transaction.retryCount = 2;
	transaction.callback = 0;
	transaction.writeData[0] = ((regAddr >> 8) & 0xFF);
	transaction.writeData[1] = (regAddr & 0xFF);
	transaction.writeData[2] = ((data >> 8) & 0xFF);
	transaction.writeData[3] = (data & 0xFF);
	transaction.writeLength = 4;
	transaction.readLength = 0;

	I2C_Transact(&transaction);
}

void VL6180_StartLog(vl6180_t* vl6180_handle, uint16_t period){
	Task_Schedule(VL6180_Log, vl6180_handle, period, period);
}

void VL6180_Log(vl6180_t* vl6180_handle){
	LogMsg(sys_id, "Address: %x Distance: %d", vl6180_handle->address, vl6180_handle->distance);
}

void VL6180_StopLog(vl6180_t* vl6180_handle){
	Task_Remove(VL6180_Log, vl6180_handle);
}


