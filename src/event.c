#include "event.h"

typedef void (*event_handler_ptr_t)(void);

/// event structure
typedef struct event_t {
	uint8_t* flag; ///< flag used to trigger function
	event_handler_ptr_t fn_ptr; ///< function pointer
} event_t;

event_t events[MAX_EVENTS];
uint8_t num_events;

void Event_Init(void){
	// use flag so module only gets initialized once
	static uint8_t init_flag = 0;
	if(init_flag) return;

	init_flag = 1;
	num_events = 0;
}

void Event_Register(uint8_t* flag, void(*fn)(void)){
	Event_Init();
	if(num_events < MAX_EVENTS){
		events[num_events].flag = flag;
		events[num_events].fn_ptr = fn;
		num_events++;
	}
}

void Event_Unregister(void(*fn_ptr)(void)){
	volatile uint16_t i, j;
	// loop through the array to find the function pointer (fn)
	// when found, shift remaining array items left to overwrite (fn)
	for(i = 0; i < num_events; i++) {
		if(events[i].fn_ptr == fn_ptr) {
			for(j = i+1; j < num_events; j++) {
				events[j-1] = events[j];
			}
			num_events--;
			return;
		}
	}
}

void Event_Tick(void){
	uint8_t i;
	
	for(i = 0; i < num_events; i++){
		if(*events[i].flag){
			events[i].fn_ptr();
			*events[i].flag = 0;
		}
	}
}
