#include "system.h"
#include "eeprom.h"

uint16_t eeprom[EEPROM_SIZE];

void hal_EEPROM_Init(void) {
	
}

uint16_t hal_EEPROM_Read(uint16_t address) {
	if(address >= EEPROM_SIZE) return 0;
	return eeprom[address];
}

void hal_EEPROM_Write(uint16_t address, uint16_t data) {
	if(address >= EEPROM_SIZE) return;
	eeprom[address] = data;
}
