#include "bluefruit.h"

void bluefruit_Init(void){
	UART_Printf(BLUEFRUIT_CHANNEL, "\r\n");
}

void bluefruit_SwitchMode(void){
	UART_Printf(BLUEFRUIT_CHANNEL, "+++\r\n");
}

void bluefruit_AT(void){
	UART_Printf(BLUEFRUIT_CHANNEL, "AT\r\n");
}
void bluefruit_ATZ(void){
	UART_Printf(BLUEFRUIT_CHANNEL, "ATZ\r\n");
}

void bluefruit_FactoryReset(void){
	UART_Printf(BLUEFRUIT_CHANNEL, "AT+FACTORYRESET\r\n");
}
void bluefruit_DFU(void){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+DFU\r\n");
}

void bluefruit_GAPSetAdvData(char * data){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GAPSETADVDATA=%s\r\n", data);
}

void bluefruit_GATTClear(void){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTCLEAR\r\n");
}

void bluefruit_GATTAddService(char * uuid){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTADDSERVICE=UUID=%s\r\n", uuid);
}

void bluefruit_GATTAddService128(char * uuid){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTADDSERVICE=UUID128=%s\r\n", uuid);
}

void bluefruit_GATTAddChar(char * uuid, char * properties, uint16_t min_length, uint16_t max_length, char * value){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTADDCHAR=UUID=%s,PROPERTIES=%s,MIN_LEN=%d,MAX_LEN=%d,VALUE=%s\r\n",
			uuid, properties, min_length, max_length, value);
}

void bluefruit_GATTAddChar128(char * uuid, char * properties, uint16_t min_length, uint16_t max_length, char * value){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTADDCHAR=UUID128=%s,PROPERTIES=%s,MIN_LEN=%d,MAX_LEN=%d,VALUE=%s\r\n",
				uuid, properties, min_length, max_length, value);
}

void bluefruit_GATTWriteChar(uint8_t id, char * data){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTCHAR=%d,%s\r\n", id, data);
}
