#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "big_int.h"

void big_init(big_int_t *big, uint8_t size) {
  big->size = size;
  big->bytes = malloc(sizeof(*big->bytes)*big->size);
  big->neg = FALSE;
  uint8_t i;
  for(i=0;i<big->size;i++) big->bytes[i] = 0;
}

void big_set(big_int_t *big, char *hex, uint8_t size) {

}

void big_copy(big_int_t *a, big_int_t *b) {
  uint8_t i;
  for(i=0;i<a->size;i++) b->bytes[i] = a->bytes[i];
}

void big_print(big_int_t *big) {
  uint8_t i;
  if(big->neg==TRUE) printf("-");
  for(i=big->size-1;i>=0;i--) {
    printf("%c",to_hex(big->bytes[i]>>4));
    printf("%c",to_hex(big->bytes[i]&0x0F));
    if(i==0) break;
  }
}

char to_hex(uint8_t num) {
  switch(num) {
    case 0: return '0'; break;
    case 1: return '1'; break;
    case 2: return '2'; break;
    case 3: return '3'; break;
    case 4: return '4'; break;
    case 5: return '5'; break;
    case 6: return '6'; break;
    case 7: return '7'; break;
    case 8: return '8'; break;
    case 9: return '9'; break;
    case 10: return 'A'; break;
    case 11: return 'B'; break;
    case 12: return 'C'; break;
    case 13: return 'D'; break;
    case 14: return 'E'; break;
    case 15: return 'F'; break;
  }
  return '0';
}

void big_resize(big_int_t *big, uint8_t size) {
  big->bytes = realloc(big->bytes,sizeof(*big->bytes)*big->size);
  if(size>big->size) {
    uint8_t i;
    for(i=big->size;i<size;i++) big->bytes[i] = 0;
  }
  big->size = size;
}

void big_norm(big_int_t *big) {
  uint8_t i;
  i = big->size-1;
  while(big->bytes[i]==0) i--;
  if(i==0) i=1;
  if(i<big->size-1) {
    big->size = i+1;
    big_resize(big,big->size);
  }
}

void big_kill(big_int_t *big) {
  free(big->bytes);
}

void big_neg(big_int_t *big) {
  if(big->neg) big->neg = FALSE;
  else big->neg = TRUE;
}

//~ void big_twoscomp(big_int_t *big) {
  //~ uint8_t i;
  //~ if(big->neg) {
    //~ big->neg = FALSE;
    //~ for(i=0;i<big->size;i++) big->bytes[i] = ~(big->bytes[i]-1);
  //~ }
  //~ else {
    //~ big->neg = TRUE;
    //~ for(i=0;i<big->size;i++) big->bytes[i] = ~big->bytes[i]+1;
  //~ }
//~ }

uint8_t big_cmp(big_int_t *a, big_int_t *b) {
  if(a->size>b->size) return GRT_A;
  if(b->size>a->size) return GRT_B;
  uint8_t i;
  for(i=a->size-1;i>=0;i--) {
    if(a->bytes[i]>b->bytes[i]) return GRT_A;
    if(i==0) break;
  }
  if(b->bytes[i]>a->bytes[i]) return GRT_B;
  return EQUAL;
}

void big_add(big_int_t *a, big_int_t *b, big_int_t *ans) {
  uint8_t i,min;
  int8_t carry;
  int16_t sum;
  big_int_t *max;
  carry = 0;
  if(a->size>b->size) {
    min = b->size;
    max = a;
  }
  else {
    min = a->size;
    max = b;
  }
  big_init(ans,max->size);
  i = 0;
  if(a->neg==b->neg) {
    while(i<min || (i<max->size && carry==1)) {
      if(i<min) sum = a->bytes[i]+b->bytes[i]+carry;
      else sum = max->bytes[i]+carry;
      if(sum>255) {
        carry = 1;
        sum -= 256;
      }
      else carry = 0;
      ans->bytes[i] = sum;
      i++;
    }
    if(carry==1) {
      big_resize(ans,ans->size+1);
      ans->bytes[i] = carry;
    }
    if(a->neg) big_neg(ans);
  }
  else {
    big_int_t *great, *less;
    uint8_t cmp;
    cmp = big_cmp(a,b);
    if(cmp==GRT_A) {
      great = a;
      less = b;
    }
    else {
      great = b;
      less = a;
    }
    while(i<min || (i<max->size && carry==-1)) {
      if(i<min) sum = great->bytes[i]-less->bytes[i];
      else sum = great->bytes[i]+carry;
      if(sum<0) {
        carry = -1;
        sum = 256-sum;
      }
      else carry = 0;
      ans->bytes[i] = sum;
      i++;
    }
    if(carry==1) printf("ERROR: UNREACHABLE CARRY");
    if((a->neg && cmp==GRT_A) || (b->neg && cmp==GRT_B)) big_neg(ans);
  }
  big_norm(ans);
}

void big_sub(big_int_t *a, big_int_t *b, big_int_t *ans) {
  big_neg(b);
  big_add(a,b,ans);
  big_neg(b);
}

void big_mult(big_int_t *a, big_int_t *b, big_int_t *ans) {

}

uint8_t big_mod(big_int_t *big, uint8_t mod) {

}

void big_powmod(big_int_t *big, uint8_t exp, uint8_t mod, big_int_t *ans) {

}

//~ int main() {
  //~ big_int_t num;
  //~ big_init(&num,32);
  //~ uint16_t i;
  //~ for(i=0;i<32;i++) {
    //~ num.bytes[i] = 0;
    //~ printf("%d",num.bytes[i]);
  //~ }
  //~ printf("\n%d",i);
  //~ return 0;
//~ }