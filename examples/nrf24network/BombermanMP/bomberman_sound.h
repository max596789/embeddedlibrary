/* 
 * File:   bomberman_sound.h
 * Author: George
 *
 * Created on April 29, 2015, 12:23 PM
 */

#ifndef BOMBERMAN_SOUND_H
#define	BOMBERMAN_SOUND_H

#include <stdint.h>

enum bomberman_sounds{
    BOMBERMAN_SOUND_MAIN_THEME = 10,
    BOMBERMAN_SOUND_BATTLE_THEME,
    BOMBERMAN_SOUND_GAME_START,
    BOMBERMAN_SOUND_BOMB,
    BOMBERMAN_SOUND_ITEM,
    BOMBERMAN_SOUND_DIE,
    BOMBERMAN_SOUND_WINNER
};

void soundMessagePlay(uint8_t sound);

#endif	/* BOMBERMAN_SOUND_H */

