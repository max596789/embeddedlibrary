/* 
 * File:   bomberman_sound.c
 * Author: George
 *
 * Created on April 29, 2015, 12:24 PM
 */

#include "bomberman_sound.h"
#include "system.h"

void soundMessagePlay(uint8_t sound)
{
    uint8_t data[1] = {sound};
    nrf24_SendMsg(MORRIS, DRUM_MSG, data, 1 );
}