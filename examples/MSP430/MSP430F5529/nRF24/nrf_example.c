#include <msp430.h>
#include <stdint.h>
#include "system.h"

void SetupClock8MHz(void);

#define RF_SPI_CH SPI_B0

void RF1_CE(uint8_t out);
void RF1_CSN(uint8_t out);
void RF1_PollIRQ(void);

void RF2_CE(uint8_t out);
void RF2_CSN(uint8_t out);
void RF2_PollIRQ(void);

void RF1_Init(void);
void RF2_Init(void);

void RF1_RxPayloadHandler(uint8_t * data, uint8_t length);
void RF1_AckPayloadHandler(uint8_t * data, uint8_t length);
void RF1_AckReceivedHandler(void);
void RF1_MaxRetriesHandler(void);

void RF2_RxPayloadHandler(uint8_t * data, uint8_t length);
void RF2_AckPayloadHandler(uint8_t * data, uint8_t length);
void RF2_AckReceivedHandler(void);
void RF2_MaxRetriesHandler(void);

void RF_Test(void);

#define RF_ADDRESS 0xF0F0F0F0E1LL

nrf24_t RF1;
nrf24_t RF2;

char tx_buf[13] = {"Hello RF2!\r\n"};
char retry_buf[15] = {"Hello Retry!\r\n"};
char ack_buf[10] = {"Hi RF1!\r\n"};

/*
 * main.c
 */
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    SetupClock8MHz();

    DisableInterrupts();

    Timing_Init();
    Task_Init();
    UART_Init(1);

    spi_settings_t spi_settings;
    spi_settings.channel = RF_SPI_CH;
    spi_settings.bit_rate = 100000;
    spi_settings.hal_settings.char7bit = 0;
    spi_settings.hal_settings.msb_first = 1;
    spi_settings.mode = 1;
    SPI_Init(&spi_settings);

    EnableInterrupts();

    RF1_Init();
    RF2_Init();

	nRF24_OpenTxPipe(&RF1, RF_ADDRESS);

	nRF24_OpenRxPipe(&RF2, 1, RF_ADDRESS);
	nRF24_StartListening(&RF2);

	Task_Schedule(RF_Test, 0, 500, 0);

	while(1){
		SystemTick();
		RF1_PollIRQ();
		RF2_PollIRQ();
	}
}


/* MSP430F5529 RF1:
* - MISO P3.1
* - MOSI P3.0
* - SCK  P3.2
* - CS   P2.3
* - CE   P2.4
* - IRQ  P2.5
*/

void RF1_Init(void){

    //Setup IRQ, CE, CSN
    P2DIR |= BIT3 | BIT4; // CE, CSN as output
    P2DIR &= ~BIT5; // IRQ as input

    RF1.ce = RF1_CE;
    RF1.csn = RF1_CSN;
    RF1.spi_channel = RF_SPI_CH;
    RF1.ReceivedPayload = RF1_RxPayloadHandler;
    RF1.AckPayloadReceived = RF1_AckPayloadHandler;
    RF1.AckReceived = RF1_AckReceivedHandler;
    RF1.MaxRetriesHit = RF1_MaxRetriesHandler;

    nRF24_Init(&RF1);
    nRF24_SetRetries(&RF1, 0xF, 0x01);
    nRF24_SetChannel(&RF1, 105);
}

void RF1_CE(uint8_t out){
	P2OUT = (P2OUT & ~BIT4) | (out << 4);
}

void RF1_CSN(uint8_t out){
	P2OUT = (P2OUT & ~BIT3) | (out << 3);
}

void RF1_PollIRQ(void){
	static uint8_t pin_state = 1;
	uint8_t new_state = (P2IN & BIT5) >> 5;

	if( (new_state != pin_state) && !new_state) {
		nRF24_ISR(&RF1);
	}
	pin_state = new_state;
}

/* MSP430F5529 RF2:
* - MISO P3.1
* - MOSI P3.0
* - SCK  P3.2
* - CS   P2.2
* - CE   P7.0
* - IRQ  P6.4
*/
void RF2_Init(void){

    //Setup IRQ, CE, CSN
    P7DIR |= BIT0; // CE as output
    P2DIR |= BIT2; // CSN as output
    P6DIR &= ~BIT4; // IRQ as input

    RF2.ce = RF2_CE;
    RF2.csn = RF2_CSN;
    RF2.spi_channel = RF_SPI_CH;
    RF2.ReceivedPayload = RF2_RxPayloadHandler;
    RF2.AckPayloadReceived = RF2_AckPayloadHandler;
    RF2.AckReceived = RF2_AckReceivedHandler;
    RF2.MaxRetriesHit = RF2_MaxRetriesHandler;

    nRF24_Init(&RF2);
    nRF24_SetRetries(&RF2, 0xF, 0x01);
    nRF24_SetChannel(&RF2, 105);
}

void RF2_CE(uint8_t out){
	P7OUT = (P7OUT & ~BIT0) | out;
}

void RF2_CSN(uint8_t out){
	P2OUT = (P2OUT & ~BIT2) | (out << 2);
}

void RF2_PollIRQ(void){
	static uint8_t pin_state = 1;
	uint8_t new_state = (P6IN & BIT4) >> 4;

	if( (new_state != pin_state) && !new_state) {
		nRF24_ISR(&RF2);
	}
	pin_state = new_state;
}


void RF_Test(void){
	nRF24_Write(&RF1, &tx_buf[0], 12);
}

void RF1_AckPayloadHandler(uint8_t * data, uint8_t length){
	UART_Write(UART1, data, length);
}

void RF1_RxPayloadHandler(uint8_t * data, uint8_t length) {
	UART_Write(UART1, data, length);
	nRF24_WriteAck(&RF1, &ack_buf[0], 9, 1);
}

void RF1_AckReceivedHandler(void){
	Task_Queue(RF_Test, 0);
}

void RF1_MaxRetriesHandler(void){
	nRF24_FlushTx(&RF1);
	Task_Queue(RF_Test, 0);
}

void RF2_AckPayloadHandler(uint8_t * data, uint8_t length){
	UART_Write(UART1, data, length);
}

void RF2_RxPayloadHandler(uint8_t * data, uint8_t length) {
	UART_Write(UART1, data, length);
	nRF24_WriteAck(&RF2, &ack_buf[0], 9, 1);
}

void RF2_AckReceivedHandler(void){
	Task_Queue(RF_Test, 0);
}

void RF2_MaxRetriesHandler(void){
	nRF24_FlushTx(&RF2);
	Task_Queue(RF_Test, 0);
}

void SetupClock8MHz(void){
	P5SEL |= BIT2+BIT3;
	UCSCTL6 &= ~XT2OFF; // Enable XT2
	UCSCTL6 &= ~XT2BYPASS;
	UCSCTL3 = SELREF__XT2CLK; // FLLref = XT2
	UCSCTL4 |= SELA_2 + SELS__DCOCLKDIV + SELM__DCOCLKDIV;

	UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx
	// Loop until XT1,XT2 & DCO stabilizes - In this case only DCO has to stabilize
	do
	{
	UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
										// Clear XT2,XT1,DCO fault flags
	SFRIFG1 &= ~OFIFG;                      // Clear fault flags
	}while (SFRIFG1&OFIFG);                   // Test oscillator fault flag

	// Disable the FLL control loop
	__bis_SR_register(SCG0);

	// Select DCO range 8MHz operation
	UCSCTL1 = DCORSEL_4;
	/* Set DCO Multiplier for 8MHz
	(N + 1) * FLLRef = Fdco
	(1 + 1) * 4MHz = 8MHz  */
	UCSCTL2 = FLLD0 + FLLN0;
	// Enable the FLL control loop
	__bic_SR_register(SCG0);

  /* Worst-case settling time for the DCO when the DCO range bits have been
	 changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
	 UG for optimization.
	 32 x 32 x 8 MHz / 32,768 Hz = 250000 = MCLK cycles for DCO to settle */
  __delay_cycles(250000);

	// Loop until XT1,XT2 & DCO stabilizes - In this case only DCO has to stabilize
	do {
		// Clear XT2,XT1,DCO fault flags
		UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
		// Clear fault flags
		SFRIFG1 &= ~OFIFG;
	} while (SFRIFG1 & OFIFG); // Test oscillator fault flag
}

